require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr == []
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |long_string| substring?(long_string, substring) }
end

def substring?(string, substring)
  string.include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  non_unique_letters = string.delete(' ').chars.select do |char|
    string.count(char) > 1
  end
  non_unique_letters.sort.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.delete('.,!?:;').split
  sorted_words = words.sort_by(&:length)
  [sorted_words[-1], sorted_words[-2]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').reject { |letter| string.include?(letter) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |yr| not_repeat_year?(yr) }
end

def not_repeat_year?(year)
  yr_digits = year.to_s.split('')
  yr_digits == yr_digits.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.uniq.select { |song| no_repeats?(song, songs) }
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, index|
    if song == song_name
      return false if song == songs[index + 1]
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  return '' unless string.downcase.include?('c')
  words = string.delete('.,:;!?').split
  sorted_words = words.sort_by { |word| c_distance(word) }
  sorted_words.first
end

def c_distance(word)
  return 999 unless word.downcase.include?('c')
  word.reverse.index('c')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  repeated_num_ranges = []
  range = []
  arr.each_with_index do |num, index|
    if num == arr[index + 1]
      range << index
    elsif range != []
      range << index
      repeated_num_ranges << [range[0], range[-1]]
      range = []
    end
  end
  repeated_num_ranges
end
